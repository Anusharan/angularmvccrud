﻿//var app;  
(function() {  
    'use strict'; //Defines that JavaScript code should be executed in "strict mode"  
    angular.module('myapp',['ngRoute'])

        .config(['$routeProvider',
            function ($routeProvider) {
            $routeProvider.
                when('/StudentAdd', {
                templateUrl: '../../ScriptsNg/View/StudentAdd.html',
                controller: 'StudentCtrl'
                })
                .when('/StudentEdit', {
                    templateUrl: '../../ScriptsNg/View/StudentEdit.html',
                    controller: 'StudentCtrl'
                })
                .when('/StudentList', {
                    templateUrl: '../../ScriptsNg/View/StudentList.html',
                    controller: 'StudentCtrl'
                })
                .otherwise({
                    redirectTo: '/'
                })
        }]);
})(); 