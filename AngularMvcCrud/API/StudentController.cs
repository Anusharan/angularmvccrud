﻿using AngularMvcCrud.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace AngularMvcCrud.API
{
    [RoutePrefix("api/Student")]
    public class StudentController : ApiController
    {
        DbEntities dbContext = null;

        public StudentController()
        {
            dbContext = new DbEntities();
        }

        [Route("SaveStudent")]
        public HttpResponseMessage SaveStudent (tblStudent student)
        {
            int result = 0;
            try
            {
                dbContext.tblStudents.Add(student);
                dbContext.SaveChanges();
                result = 1;
            }
            catch(Exception e)
            {
                result = 0;
            }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        [Route("GetAllStudents")]
        [ResponseType(typeof(tblStudent))]
        [HttpGet]
        public List<tblStudent> GetAllStudents()
        {
            List<tblStudent> students = null;
            try
            {
                students = dbContext.tblStudents.ToList();
            }
            catch(Exception e)
            {
                students = null;
            }
            return students;
        }

        [Route("GetStudentByID/{studentID:int}")]
        [ResponseType(typeof(tblStudent))]
        [HttpGet]
        public tblStudent GetStudentByID(int studentID)
        {
            tblStudent student = null;
            try
            {
                student = dbContext.tblStudents.Where(x => x.StudentID == studentID).SingleOrDefault();
            }
            catch (Exception e)
            {
                student = null;
            }
            return student;
        }

        [ResponseType(typeof(tblStudent))]
        [HttpPut]
        public HttpResponseMessage UpdateStudent(tblStudent student)
        {
            int result = 0;
            try
            {
                dbContext.tblStudents.Attach(student);
                dbContext.Entry(student).State = EntityState.Modified;
                dbContext.SaveChanges();
                result = 1;
            }
            catch (Exception e)
            {
                result = 0;
            }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        [Route("DeleteStudent/{id:int}")]
        [ResponseType(typeof(tblStudent))]
        [HttpGet]
        public HttpResponseMessage DeleteStudent(int id)
        {
            int result = 0;
            try
            {
                var student = dbContext.tblStudents.Where(x => x.StudentID == id).FirstOrDefault();
                dbContext.tblStudents.Attach(student);
                dbContext.tblStudents.Remove(student);
                dbContext.SaveChanges();
                result = 1;
            }
            catch (Exception e)
            {
                result = 0;
            }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }
    }
}
