﻿(function () {
    'use strict';

    angular.module('myapp')
        .controller('StudentCtrl', StudentCtrl);

    StudentCtrl.$inject = ['CrudService'];

    function StudentCtrl(CrudService) {
        var vm = this;
        var baseUrl = '/api/Student/';
        vm.btnText = "Save";
        vm.studentID = 0;

        vm.SaveUpdate = function () {
            var student = {
                FirstName: vm.firstName,
                LastName: vm.lastName,
                Email: vm.email,
                Address: vm.address,
                StudentID: vm.studentID
            }
            if (vm.btnText == "Save") {
                var apiRoute = baseUrl + 'SaveStudent/';
                var saveStudent = CrudService.post(apiRoute, student);
                saveStudent.then(function (response) {
                    if (response.data != "") {
                        alert("Data Saved Successfully");
                        vm.GetStudents();
                        vm.Clear();
                    }
                    else {
                        alert("Error Saving Data");
                    }
                },
                    function (error) {
                        console.log("Error: " + error);
                    });
            } else {
                var apiRoute = baseUrl + 'UpdateStudent/';
                var UpdateStudent = CrudService.put(apiRoute, student);
                UpdateStudent.then(function (response) {
                    if (response.data != "") {
                        alert("Data Update Successfully");
                        vm.GetStudents();
                        vm.Clear();
                    } else {
                        alert("Some error");
                    }
                }, function (error) {
                    console.log("Error: " + error);
                });
            }

        }

        function GetStudents() {
            var apiRoute = baseUrl + 'GetAllStudents/';
            var students = CrudService.getAll(apiRoute);
            students.then(function (response) {
                vm.students = response.data;
            }, function (error) {
                console.log("Error: " + error);
            });
        }
        GetStudents();

        vm.GetStudentByID = function (dataModel) {
            var apiRoute = baseUrl + 'GetStudentByID';
            var student = CrudService.getbyID(apiRoute, dataModel.StudentID);
            student.then(function (response) {
                vm.studentID = response.data.StudentID;
                vm.firstName = response.data.FirstName;
                vm.lastName = response.data.LastName;
                vm.email = response.data.Email;
                vm.address = response.data.Address;
                vm.btnText = "Update";
            }, function (error) {
                console.log("Error: " + error);
            });
        }

        vm.DeleteStudent = function (dataModel) {
            var apiRoute = baseUrl + 'DeleteStudent/' + dataModel.StudentID;
            var deleteStudent = CrudService.delete(apiRoute);
            deleteStudent.then(function (response) {
                if (response.data != "") {
                    alert("Data Deleted Successfully");
                    vm.GetStudents();
                    vm.Clear();
                }
                else {
                    alert("Error Deleting Data");
                }
            },
                function (error) {
                    console.log("Error: " + error);
                });
        }
    }
})();

//app.controller('StudentCtrl', ['$scope', 'CrudService',
//    function ($scope, CrudService) {
//        var baseUrl = '/api/Student/';
//        $scope.btnText = "Save";
//        $scope.studentID = 0;

//        $scope.SaveUpdate = function () {
//            var student = {
//                FirstName: $scope.firstName,
//                LastName: $scope.lastName,
//                Email: $scope.email,
//                Address: $scope.address,
//                StudentID: $scope.studentID
//            }
//            if ($scope.btnText == "Save") {
//                var apiRoute = baseUrl + 'SaveStudent/';
//                var saveStudent = CrudService.post(apiRoute, student);
//                saveStudent.then(function (response) {
//                    if (response.data != "") {
//                        alert("Data Saved Successfully");
//                        $scope.GetStudents();
//                        $scope.Clear();
//                    }
//                    else {
//                        alert("Error Saving Data");
//                    }
//                },
//                    function (error) {
//                        console.log("Error: " + error);
//                    });
//            } else {
//                var apiRoute = baseUrl + 'UpdateStudent/';
//                var UpdateStudent = CrudService.put(apiRoute, student);
//                UpdateStudent.then(function (response) {
//                    if (response.data != "") {
//                        alert("Data Update Successfully");
//                        $scope.GetStudents();
//                        $scope.Clear();
//                    } else {
//                        alert("Some error");
//                    }
//                }, function (error) {
//                    console.log("Error: " + error);
//                });
//            }
            
//        }

//        $scope.GetStudents = function () {
//            var apiRoute = baseUrl + 'GetAllStudents/';
//            var students = CrudService.getAll(apiRoute);
//            students.then(function (response) {
//                $scope.students = response.data;
//            }, function (error) {
//                console.log("Error: " + error);
//            });
//        }
//        $scope.GetStudents();

//        $scope.GetStudentByID = function (dataModel) {
//            var apiRoute = baseUrl + 'GetStudentByID';
//            var student = CrudService.getbyID(apiRoute, dataModel.StudentID);
//            student.then(function (response) {
//                $scope.studentID = response.data.StudentID;
//                $scope.firstName = response.data.FirstName;
//                $scope.lastName = response.data.LastName;
//                $scope.email = response.data.Email;
//                $scope.address = response.data.Address;
//                $scope.btnText = "Update";
//            }, function (error) {
//                console.log("Error: " + error);
//            });
//        }

//        $scope.DeleteStudent = function (dataModel) {
//            var apiRoute = baseUrl + 'DeleteStudent/' + dataModel.StudentID;
//            var deleteStudent = CrudService.delete(apiRoute);
//            deleteStudent.then(function (response) {
//                if (response.data != "") {
//                    alert("Data Deleted Successfully");
//                    $scope.GetStudents();
//                    $scope.Clear();
//                }
//                else {
//                    alert("Error Deleting Data");
//                }
//            },
//                function (error) {
//                    console.log("Error: " + error);
//                });
//        }
//    }
//]);