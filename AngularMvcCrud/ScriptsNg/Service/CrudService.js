﻿(function () {
    'use strict';
    angular.module('myapp')
        .service('CrudService', CrudService);

    CrudService.$inject = ['$http'];

    function CrudService($http) {
        var urlGet = '';
        this.post = function (apiRoute, Model) {
            var request = $http({
                method: "post",
                url: apiRoute,
                data: Model
            });
            return request;
        }

        this.put = function (apiRoute, Model) {
            var request = $http({
                method: "put",
                url: apiRoute,
                data: Model
            });
            return request;
        }

        this.delete = function (apiRoute) {
            var request = $http({
                method: "get",
                url: apiRoute
            });
            return request;
        }

        this.getAll = function (apiRoute) {
            console.log('insiide getall', apiRoute);
            urlGet = apiRoute;
            return $http.get(urlGet);
        }

        this.getbyID = function (apiRoute, studentID) {
            urlGet = apiRoute + '/' + studentID;
            return $http.get(urlGet);
        }
    }
})();

//app.service('CrudService', function ($http) {
//    var urlGet = '';
//    this.post = function(apiRoute, Model) {
//        var request = $http({
//            method: "post",
//            url: apiRoute,
//            data: Model
//        });
//        return request;
//    }

//    this.put = function (apiRoute, Model) {
//        var request = $http({
//            method: "put",
//            url: apiRoute,
//            data: Model
//        });
//        return request;
//    } 

//    this.delete = function (apiRoute) {
//        var request = $http({
//            method: "get",
//            url: apiRoute
//        });
//        return request;
//    }

//    this.getAll = function (apiRoute) {
//        urlGet = apiRoute;
//        return $http.get(urlGet);
//    }

//    this.getbyID = function (apiRoute, studentID) {
//        urlGet = apiRoute + '/' + studentID;
//        return $http.get(urlGet);
//    }
//});